Design an Online piggy bank

1. Frontend web app(Modern web framework such as React/Vue/Angular or plain javascript whichever is your preference). Please refer to the wire frame below.
2. Data should be mocked on the frontend.

Features-

- Users can add money to the piggy bank
- Users can see all transactions in the piggy bank
- To withdraw money from a piggy bank, users have to break the piggy bank. When user breaks the piggy bank he will get all the money from the piggy bank.
- Once the piggy bank is broken, users can start a new piggy bank.
- At a time there can be only one piggy bank in the system. Users can create a new piggy bank only when the previous one is destroyed.

Notes:

- The assignment will be judged based on code quality, and software practices used.
- You can avoid authentication and authorization.
- Feel free to cut it short. The assignment need not be production level. But you should be able to explain your design.
- You can choose any technology and design methodology. But be ready to explain the design decision.
- You can keep the UI simple, we are more interested in component design.
- Webapp can be multi page or a single page application.
- Please refer to the wireframe below to get an idea of the system. Wireframe is intentionally kept simple. Please feel free to make your design decisions and assumptions with respect to the styling, UI and UX.

![alt_text](https://i.imgur.com/YSHqeQM.png "image_tooltip")
